<?php
include 'db_connect.php';

echo $_GET['rn'];
$sql = "DELETE FROM rooms WHERE rn = ?";

if($stmt = $mysqli->prepare($sql)){

    $stmt->bind_param("i", $_GET['rn']);

    if($stmt->execute()){

        $stmt->close();

        header('Location: home.php?action=notif');
 
    }else{
        die("Error: Unable to Delete!!");
    }
 
}
?>