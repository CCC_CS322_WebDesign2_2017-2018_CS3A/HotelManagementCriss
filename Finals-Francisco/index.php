<!DOCTYPE HTML>
<html>
    <head>
        <title> Online Hotel Reservation </title>
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
<body>
 
<h1> Online Hotel Reservation </h1>
 
<?php

include 'db_connect.php';
 
$action = isset($_GET['action']) ? $_GET['action'] : "";

if($action=='deleted'){
}
 
$query = "select * from rooms";
$result = $mysqli->query( $query );
 
$num_results = $result->num_rows;

    echo "<ul>";
        echo "<li><a href='index.php'> HOME </a></li>";
        echo "<li><a href='glogin.php' class='login'> GUESS LOGIN </a></li>";
        echo "<li><a href='login.php' class='login'> ADMIN LOGIN </a></li>";
    echo "</ul>";
 
if( $num_results ){

	echo "<h1> Room List </h1>";

    echo "<table border='1'>";

        echo "<tr>";
            echo "<th>Room Number</th>";
            echo "<th>Room Type</th>";
            echo "<th>Room Status</th>";
            echo "<th>Name Of Client</th>";
            echo "<th>Check In Date</th>";
            echo "<th>Check Out Date</th>";
        echo "</tr>";

    while( $row = $result->fetch_assoc() ){

        extract($row);

        echo "<tr>";
            echo "<td>{$rn}</td>";
            echo "<td>{$rt}</td>";
            echo "<td>{$rs}</td>";
            echo "<td>{$noc}</td>";
            echo "<td>{$cid}</td>";
            echo "<td>{$cod}</td>";
        echo "</tr>";
    }

    echo "</table>";
 
}

else{
    echo "<p> No records found </p>";
}

$result->free();
$mysqli->close();
?>
 
<script type='text/javascript'>
function delete_item( rn){
 
    var answer = confirm('Are you sure?');

    if ( answer ){

        window.location = 'delete.php?rn=' + rn;
    }
}
</script>
 
</body>
</html>