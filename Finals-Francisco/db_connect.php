<?php

$host = "localhost";
$db_name = "rooms";
$username = "root";
$password = "";

$mysqli = new mysqli($host, $username, $password, $db_name);

if(mysqli_connect_errno()) {
    echo "Error: Could not connect to database.";
    exit;
}
?>