-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2018 at 02:00 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rooms`
--

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `rn` int(11) NOT NULL,
  `rt` varchar(100) NOT NULL,
  `rs` varchar(100) NOT NULL,
  `noc` varchar(100) NOT NULL,
  `cid` varchar(100) NOT NULL,
  `cod` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`rn`, `rt`, `rs`, `noc`, `cid`, `cod`) VALUES
(1, 'Deluxe', 'Vacant', '', '', ''),
(2, 'Deluxe', 'Vacant', '', '', ''),
(3, 'Deluxe', 'Vacant', '', '', ''),
(4, 'Deluxe', 'Vacant', '', '', ''),
(5, 'Deluxe', 'Vacant', '', '', ''),
(6, 'Luxury', 'Vacant', '', '', ''),
(7, 'Luxury', 'Vacant', '', '', ''),
(8, 'Luxury', 'Vacant', '', '', ''),
(9, 'Luxury', 'Vacant', '', '', ''),
(10, 'Luxury', 'Vacant', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`rn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `rn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
