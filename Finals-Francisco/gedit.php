<!DOCTYPE HTML>
<html>
    <head>
        <title> Online Hotel Reservation </title>
        <link rel="stylesheet" type="text/css" href="css/edit.css">
    </head>
<body>
 
<h1> Update Rooms </h1>
 
<?php

include 'db_connect.php';

if($_POST){

    $sql = "UPDATE
                rooms
            SET
                rt = ?,
                rs = ?,
                noc = ?,
                cid  = ?,
                cod = ?
            WHERE
                rn= ?";
 
    $stmt = $mysqli->prepare($sql);
 
    $stmt->bind_param(
        'sssssi',
        $_POST['rt'],
        $_POST['rs'],
        $_POST['noc'],
        $_POST['cid'],
        $_POST['cod'],
        $_POST['rn']
    );

    if($stmt->execute()){
        echo "<p> Success: Room Successfully Reserved!! </p>";

        $stmt->close();
    }else{
        die("<p> Error: Unable to Reserve the Room!! </p>");
    }
}
 
$sql = "SELECT
            rn, rt, rs, noc, cid, cod
        FROM
            rooms
        WHERE
           rn = \"" . $mysqli->real_escape_string($_GET['rn']) . "\"
        LIMIT
            0,1";

$result = $mysqli->query( $sql );

$row = $result->fetch_assoc();

extract($row);

$result->free();
$mysqli->close();
?>

<form action='gedit.php?rn=<?php echo $rn; ?>' method='post' border='0'>
    <table>
        <tr>
            <td> Room Type: </td>
            <td><input type='text' name='rt' value='<?php echo $rt;  ?>' /></td>
        </tr>
        <tr>
            <td> Room Status: </td>
            <td><input type='text' name='rs' value='<?php echo $rs;  ?>' /></td>
        </tr>
        <tr>
            <td> Name of Client: </td>
            <td><input type='text' name='noc' value='<?php echo $noc;  ?>' /></td>
        </tr>
        <tr>
            <td> Check in Date: </td>
            <td><input type='text' name='cid' value='<?php echo $cid;  ?>' /></td>
        <tr>
        <tr>
            <td> Check out Date: </td>
            <td><input type='text' name='cod' value='<?php echo $cod;  ?>' /></td>
        <tr>
            <td></td>
            <td>

                <input type='hidden' name='rn' value='<?php echo $rn ?>' />
                <input type='submit' value='Reserve' class="btn-edit" />
                <a href='guess.php'>Back to Home</a>
            </td>
        </tr>
    </table>
</form>
 
</body>
</html>