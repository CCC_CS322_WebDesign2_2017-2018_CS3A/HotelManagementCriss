<!DOCTYPE HTML>
<html>
    <head>
        <title> Online Hotel Reservation </title>
        <link rel="stylesheet" type="text/css" href="css/add.css">
    </head>
<body>
 
<h1> Adding New Room </h1>
 
<?php

if($_POST){
 
    include 'db_connect.php';
 
    $sql = "INSERT INTO
                rooms (rt, rs)
            VALUES
                (?, ?)";
 
    if($stmt = $mysqli->prepare($sql) ){
 
        $stmt->bind_param(
            "ss",
            
            $_POST['rt'],
            $_POST['rs']
        );

        if($stmt->execute()){
            echo "<p> Room Successfully Added!! </p>";
            $stmt->close();
        }else{
            die("<p> Unable to Add Room!! </p>" );
        }
 
    }else{
        die("Unable to prepare statement!!");
    }

    $mysqli->close();
}
 
?>
 <div class="container">
    <form action='add.php' method='post' border='0'>
        <img src="image/add.png">
        <div class="form-input">
            <input type="text" name="rt" placeholder="Input Room Type">
        </div>
        <div class="form-input">
            <input type="text" name="rs" placeholder="Room Status">
        </div>

        <input type="submit" name="submit" value="ADD" class="btn-confirm">
        <a href="home.php"> GO BACK </a>
</form>
 </div>
 
</body>
</html>